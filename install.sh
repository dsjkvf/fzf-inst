#!/bin/sh

if [ $1 = --vim ]
then
    curl -s https://raw.githubusercontent.com/junegunn/fzf/master/plugin/fzf.vim --create-dirs -o autoload/fzf.vim
elif [ $1 = --bin ]
then
    sys=$(uname)
    arc=$(uname -p)
    if echo $sys | grep -iq linux && [ $(echo -n $arc | tail -c 2) = 86 ]
    then
        curl -O https://bitbucket.org/dsjkvf/fzf-inst/downloads/fzf-0.24-linux-x86 && mv fzf-0.24-linux-x86 fzf && chmod 744 fzf
        # this is a pre-built binary v.0.24
        # to build one yourself, do the following:
        # git clone https://github.com/junegunn/fzf
        # cd fzf
        # GOARCH=386 go build -a -ldflags "-X main.revision= -w '-extldflags=-L/opt/local/lib -Wl,-rpath,/opt/local/lib'" -tags "" -o target/fzf
    else
        curl -s https://api.github.com/repos/junegunn/fzf/releases/latest | grep -i $sys | grep url | cut -d ':' -f2,3 | tr -d '"' | xargs curl -sL | tar xzv
    fi
else
    echo "USAGE:"
    echo "      ./install.sh --vim"
    echo "      or"
    echo "      ./install.sh --bin"
fi
